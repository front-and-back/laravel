<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    //
    public function index($name){
        return "Welcome " . $name ;
    }
    public function home(){
        $name = "Rafeef Aqraa";
        $sayHello = "Welcome";
        return view('Backend.home', compact('name','sayHello'));
    }
}

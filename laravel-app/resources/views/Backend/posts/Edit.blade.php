@extends('Backend.layout.main')

@section('title','POSTS')

@section('content')
<h1 class="mt-3 text-center">Edit Post</h1>
      <div class="container">
        <form action="/posts/{{ $posts->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Add Title</label>
                <input type="text" class="form-control" id="title" name="title" value="{{ $posts->title}}" placeholder="Type your title">
            </div>
            <div class="form-group">
                <label for="body">Add Content</label>
                <textarea class="form-control" id="content" rows="3" name="content">{{ $posts->content }}</textarea>
            </div>
            <div class="form-group">
                <label for="photo">File Photo</label>
                <input type="file" class="form-control-file" id="photo" name="photo">
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
          </form>
      </div>

@endsection
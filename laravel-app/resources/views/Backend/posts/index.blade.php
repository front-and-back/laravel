@extends('Backend.layout.main')

@section('title','INDEX')

@section('content')
    <div class="container">
        <div class="row mt-5">
            @foreach ($posts as $post)
                <div class="col-xs-6 col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img src="{{ $post->photo }}" class="card-img-top " alt="{{$post->title}}">
                        <div class="card-body">
                            <h5 class="card-title">{{ $post->title }}</h5>
                            <p class="card-text">{{ $post->content }}</p>
                            <form action="/posts/{{ $post->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                            <a href="/posts/{{ $post->id }}/edit" class="btn btn-primary">Edit</a>
                            <button class="btn btn-danger">Delete</button>
                           </form>
                        </div>
                    </div>
                </div>
            @endforeach
         </div>
         <a href="/posts/create" class="btn btn-success mt-4">Add New Post</a>
    </div>
@endsection